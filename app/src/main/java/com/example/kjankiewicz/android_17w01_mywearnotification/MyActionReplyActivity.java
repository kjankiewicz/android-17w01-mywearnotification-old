package com.example.kjankiewicz.android_17w01_mywearnotification;

import android.content.Intent;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class MyActionReplyActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_action_reply);

        TextView textViewAnsWear = (TextView) findViewById(R.id.textViewAnsWear);
        ImageView imageViewAnsWear = (ImageView) findViewById(R.id.imageViewAnsWear);

        Intent intent = getIntent();
        int intExtra = intent.getIntExtra(MyMainActivity.EXTRA_EVENT_ID, 0);

        Log.d(getLocalClassName(),"Extras: " + MyMainActivity.EXTRA_EVENT_ID + ": " + intExtra);

        switch (intExtra) {
            case MyMainActivity.mainEventId: {
                imageViewAnsWear.setImageResource(R.mipmap.ic_launcher);
                textViewAnsWear.setText(String.valueOf(intExtra));
                break;
            }
            case MyMainActivity.secondEventId: {
                imageViewAnsWear.setImageResource(R.drawable.gwiazdka);
                textViewAnsWear.setText(String.valueOf(intExtra));
                break;
            }
            case MyMainActivity.voiceReplyEventId: {
                imageViewAnsWear.setImageResource(R.drawable.reply);
                Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
                textViewAnsWear.setText(String.valueOf(intExtra) + ": " +
                        remoteInput.getCharSequence(MyMainActivity.EXTRA_VOICE_REPLY));
                break;
            }
            case MyMainActivity.choiceReplyEventId: {
                imageViewAnsWear.setImageResource(R.drawable.choice_reply);
                Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
                textViewAnsWear.setText(String.valueOf(intExtra) + ": " +
                        remoteInput.getCharSequence(MyMainActivity.EXTRA_CHOICE_REPLY));
                break;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_action_reply, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
