package com.example.kjankiewicz.android_17w01_mywearnotification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MyMainActivity extends ActionBarActivity {

    public static final String EXTRA_VOICE_REPLY = "extra_voice_reply";
    public static final String EXTRA_CHOICE_REPLY = "extra_choice_reply";
    public static final String EXTRA_EVENT_ID = "extra_event_id";

    public static final int mainEventId = 1;
    public static final int secondEventId = 2;
    public static final int voiceReplyEventId = 3;
    public static final int choiceReplyEventId = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_main);

        Button putNotification = (Button) findViewById(R.id.put_notification);

        putNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNotification();
            }
        });

    }

    private void sendNotification()
    {
        int notificationId = 001;

        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.bigText(getString(R.string.lorem_ipsum));

        // Build "main" intent for notification content
        Intent mainIntent = new Intent(this, MyActionReplyActivity.class);
        mainIntent.putExtra(EXTRA_EVENT_ID, mainEventId);
        PendingIntent mainPendingIntent = PendingIntent.getActivity(this, mainEventId, mainIntent, 0);

        // Build "second" intent for notification action
        Intent secondIntent = new Intent(this, MyActionReplyActivity.class);
        secondIntent.putExtra(EXTRA_EVENT_ID, secondEventId);
        PendingIntent secondPendingIntent = PendingIntent.getActivity(this, secondEventId, secondIntent, 0);

        NotificationCompat.Action secondAction =
                new NotificationCompat.Action.Builder(R.drawable.gwiazdka,
                        getString(R.string.wearable_action), secondPendingIntent)
                        .build();

        // Create second page notification
        NotificationCompat.BigTextStyle secondPageStyle = new NotificationCompat.BigTextStyle();
        secondPageStyle.setBigContentTitle(getString(R.string.page2_notification))
                .bigText(getString(R.string.lorem_ipsum));

        Notification secondPageNotification =
                new NotificationCompat.Builder(this)
                        .setStyle(secondPageStyle)
                        .build();

        // Build "reply" intent for notification replay action
        RemoteInput remoteReplyInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY)
                .setLabel(getString(R.string.reply_label))
                .build();

        Intent voiceReplyIntent = new Intent(this, MyActionReplyActivity.class);
        voiceReplyIntent.putExtra(EXTRA_EVENT_ID, voiceReplyEventId);
        PendingIntent voiceReplyPendingIntent =
                PendingIntent.getActivity(this, voiceReplyEventId, voiceReplyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Action voiceReplyAction =
                new NotificationCompat.Action.Builder(R.drawable.reply,
                        getString(R.string.reply_label), voiceReplyPendingIntent)
                        .addRemoteInput(remoteReplyInput)
                        .build();

        // Build "reply_choice" intent for notification replay action
        RemoteInput remoteChoiceInput = new RemoteInput.Builder(EXTRA_CHOICE_REPLY)
                .setLabel(getString(R.string.choice_reply_label))
                .setChoices(getResources().getStringArray(R.array.reply_choices))
                .build();

        Intent choiceReplyIntent = new Intent(this, MyActionReplyActivity.class);
        choiceReplyIntent.putExtra(EXTRA_EVENT_ID, choiceReplyEventId);
        PendingIntent choiceReplyPendingIntent =
                PendingIntent.getActivity(this, choiceReplyEventId, choiceReplyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Action choiceReplyAction =
                new NotificationCompat.Action.Builder(R.drawable.choice_reply,
                        getString(R.string.choice_reply_label), choiceReplyPendingIntent)
                        .addRemoteInput(remoteChoiceInput)
                        .build();

        WearableExtender wearableExtender =
                new WearableExtender()
                        .addAction(secondAction)
                        .addAction(voiceReplyAction)
                        .addAction(choiceReplyAction)
                        .setContentIcon(R.drawable.gwiazdka)
                        .setContentIconGravity(Gravity.START)
                        .addPage(secondPageNotification)
                        .setHintHideIcon(true)
                        .setBackground(BitmapFactory.decodeResource(
                                getResources(), R.drawable.panum_big));

        Notification notification =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(), R.drawable.gwiazdka))
                        .setStyle(new NotificationCompat.BigPictureStyle())
                        .setContentTitle(getString(R.string.title_notification))
                        .setContentText(getString(R.string.content_notification))
                        .setContentIntent(mainPendingIntent)
                        .extend(wearableExtender)
                        .setStyle(bigStyle)
                        .build();

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notification);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
